<?php

// Define the Panda class.
class Panda
{
    // Fields
    var $name;
    var $coat = 'fluffy';
    var $colour;

//     Constructors
//    function __construct()
//    {
//        $this->name = "DEFAULT NAME";
//    }

//    function __construct($name)
//    {
//        $this->name = $name;
//    }

    function __construct($name="DEFAULT NAME")
    {
        $this->name = $name;
    }

    // Methods
    function getCoat() {
        return $this->coat;
    }

    function getColour() {
        return $this->colour;
    }

    function withName($nameGiven) {
        $this->name = $nameGiven;
    }

    function makeNameLoud() {
        return strtoupper($this->name);
    }

    function makeNameQuiet() {
        return strtolower($this->name);
    }
}

// Main Method

class GiantPanda extends Panda {

}

$giantPanda = new GiantPanda();

// Get Coat Type
echo $giantPanda->getCoat();



//$defaultPanda = new Panda();
//$andrewPanda = new Panda("Andrew");
//echo $defaultPanda->makeNameLoud()."<br>";
//echo $defaultPanda->makeNameQuiet()."<br>";
//
//echo $andrewPanda->makeNameLoud()."<br>";
//echo $andrewPanda->makeNameQuiet()."<br>";