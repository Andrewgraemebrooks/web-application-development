<?php

// Define The Book Class
class Book
{

    // Declare Properties
    var $title;

    var $author;

    var $publisher;

    var $yearOfPublication;

    var $format = 'Paperback'; // Default Value.

    // Declare a method.
    function sayHello() {
        return 'Hello';
    }

    function summary() {
        echo 'Title: '      . $this->title      .PHP_EOL;
        echo 'Author: '     . $this->author     .PHP_EOL;
        echo 'Publisher: '  . $this->publisher  .PHP_EOL;
    }
}

// Create a new book instance.
$book = new Book;

// Set Properties
$book ->title            = 'Game of Thrones';
$book->author            = 'George R R Martin';
$book->publisher         = 'Voyager Books';
$book->yearOfPublication = 1996;

// Echo Properties.
//echo $book->title               .PHP_EOL;
//echo $book->author              .PHP_EOL;
//echo $book->publisher           .PHP_EOL;
//echo $book->yearOfPublication   .PHP_EOL;

//echo $book->sayHello();

// Output a book summary
$book->summary();