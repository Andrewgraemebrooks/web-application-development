<?php

// Create a multi-dimensional array.
$numbers = [
    'prime'         => [2, 3, 5, 7, 11],
    'fibonacci'     => [1, 1, 2, 3, 5],
    'triangular'    => [1, 3, 6, 10, 15]
];
$primes = $numbers['prime'];
//echo $primes[2];
echo $numbers['prime'][2];