<?php

// Set a string.
$value = 'pandas';

// Single quotes.
$first = 'We love $value!';

// Double quotes.
$second = "We love $value!";

// Cleaner Double Quotes
$third = "We love {$value}!";

// Output.
var_dump($first);
var_dump($second);
var_dump($third);