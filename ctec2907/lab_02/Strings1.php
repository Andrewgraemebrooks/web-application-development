<?php

// String with single quotes.
$panda = 'Pandas rule!';

// String with double quotes.
$panda = "Pandas rule!";

// Output.
echo $panda;