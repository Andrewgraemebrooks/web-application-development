<?php
$months = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];

$monthsKV = [
  "jan" => 31,
  "feb" => 28,
  "mar" => 31
];

print_r($months);

echo "<br>";

foreach($months as $month) {
    echo "$month<br>";
}

echo "<br>";

foreach($months as $key => $value) {
    echo "$key : $value <br>";
}

echo "<br>";

for ($i = 0; $i < count($months); $i++) {
    echo $months[$i]."<br>";
}