<?php

// Create an array of strings.
$pandas = array('Lushui', 'Jasmina', 'Pali');

// Create an array of integers.
$integers = [3, 6, 9, 12];

// Create an array of floats.
$floats = [1.30, 2.60, 3.90, 4.120];

// Set some variables.
$one    = 1;
$two    = 2;
$three  = 3;

// Create an array of variables.
$variables = array($one, $two, $three);