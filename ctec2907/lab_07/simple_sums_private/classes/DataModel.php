<?php
/**
 * ResultModel.php
 *
 * PHP program to perform simple calculations
 *
 * class to perform the requested calculation using the user-entered parameters
 * after validation and sanitisation
 *
 * @author CF Ingrams - cfi@dmu.ac.uk
 * @copyright De Montfort University
 *
 * @package simple_sums
 */

class DataModel
{
    private $calculation_type;
    private $value_1;
    private $value_2;
    private $retrieved_data;
    private $validate_error_flag;

    public function __construct()
    {
        $this->calculation_type = '';
        $this->value_1 = null;
        $this->value_2 = null;
        $this->retrieved_data = null;
        $this->validate_error_flag = false;
    }

    public function __destruct() {}

    public function getStoredData()
    {
        return $this->retrieved_data;
    }

    /**
     * Retrieve stored data from the database
     *
     * Return error messages if the database was not accessible.
     *
     */
    public function retrieveData()
    {
        $data_list = [];

        $database = Factory::createDatabaseWrapper();

        $query = SqlQueries::queryGetUserData();

        $parameters = [];

        $database->safeQuery($query, $parameters);

        $data_retrieval_messages = $database->getDatabaseResultMessages();
        $data_list['data-retrieval-error'] = $data_retrieval_messages['database-query-execute-error'];

        $calulation_count = $database->countRows();

        if ($calulation_count <= 0)
        {
            $data_error = true;
        }
        else
        {
            $counter = 0;
            while ($row = $database->safeFetchArray())
            {
                $data_list[$counter]['client_port'] = $row['client_port'];
                $data_list[$counter]['user_agent'] = $row['user_agent'];
                $data_list[$counter]['calculation_type'] = $row['calculation_type'];
                $data_list[$counter]['value1'] = $row['value1'];
                $data_list[$counter]['value2'] = $row['value2'];
                $data_list[$counter]['timestamp'] = $row['timestamp'];
                $counter++;
            }
        }

        $this->retrieved_data = $data_list;
    }
}

