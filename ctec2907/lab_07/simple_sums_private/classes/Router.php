<?php
/**
 * Router.php
 *
 * PHP program to perform simple calculations
 *
 * Maps the user-sourced feature name to an internal feature name
 * Then calls the relevant Controller class via a container function
 *
 * If the class definition file is not found, the Error class is invoked
 *
 * @author CF Ingrams - cfi@dmu.ac.uk
 * @copyright De Montfort University
 *
 * @package simple_sums
 */

class Router
{
    private $feature_in;
    private $feature;
    private $html_output;

    public function __construct()
    {
        $this->feature_in = '';
        $this->feature = '';
        $this->html_output = '';
    }

    public function __destruct(){}

    public function doRouting()
    {
        $this->setFeatureName();
        $this->mapFeatureName();
        $this->selectController();
    }

    public function getHtmlOutput()
    {
        return $this->html_output;
    }

    /**
     * feature name defaults to 'index' unless an alternative is received via HTTP from the client
     */
    private function setFeatureName()
    {
        if (isset($_POST['feature']))
        {
            $feature_in = $_POST['feature'];
        }
        else
        {
            if (isset($_GET['feature']))
            {
                $feature_in = $_GET['feature'];
            }
            else
            {
                $feature_in = 'index';
            }
        }
        $this->feature_in = $feature_in;
    }

    /**
     * Check to see that the feature name passed from the client is valid.
     * If not valid, chances are that a user is attempting something malicious.
     * In which case, kill the app's execution.
     */
    private function mapFeatureName()
    {
        $feature_exists = false;
        // map the passed module name to an internal application module name
        $features =
            [
                'index' => 'form',
                'display-result' => 'result',
                'retrieve-data' => 'data'
            ];

        if (array_key_exists($this->feature_in, $features))
        {
            $this->feature = $features[$this->feature_in];
            $feature_exists =  true;
        }
        else
        {
            die();
        }
        return $feature_exists;
    }

    /**
     * Select the relevant Controller class
     */
    public function selectController()
    {
        switch ($this->feature)
        {
            case 'form':
                $controller = Factory::buildObject('FormController');
                break;
            case 'result':
                $controller = Factory::buildObject('ResultController');
                break;
            case 'data':
                $controller = Factory::buildObject('DataController');
                break;
            default:
        }

        $controller->createOutput();
        $this->html_output = $controller->getHtmlOutput();

    }
}
