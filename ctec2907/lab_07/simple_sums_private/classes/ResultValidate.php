<?php
/**
 * ResultValidate.php
 *
 * PHP program to perform simple calculations
 *
 * class to validate & sanitise the user entered values
 * returns an error flag if there was a problem
 *
 * NB the values entered by the user are passed as an array
 *
 * @author CF Ingrams - cfi@dmu.ac.uk
 * @copyright De Montfort University
 *
 * @package simple_sums
 */

class ResultValidate
{
    public function __construct() {}

    public function __destruct() {}

    /**
     * Tests that every character in the entered string is a digit.  If  returns false
     * @param $value
     * @return bool|int
     */
    public function validateInteger($value)
    {
        $sanitised_integer = false;
        if (ctype_digit(($value)))
        {
            $sanitised_integer = (int)filter_var($value, FILTER_SANITIZE_NUMBER_INT);
        }
        return $sanitised_integer;
    }

    public function validateType(string $type)
    {
        $found = false;
        $calculation_type = ['addition', 'subtraction', 'multiplication', 'division'];

        if (in_array($type, $calculation_type))
        {
            $found = $type;
        }
        return $found;
    }

}
