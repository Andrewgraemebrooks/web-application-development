<?php
/**
 * ResultView.php
 *
 * PHP program to perform simple calculations
 *
 * class to create the relevant output html content, dependent upon
 * successful validation and calculation
 *
 * extends the web page template class to give a standard page layout
 *
 * @author CF Ingrams - cfi@dmu.ac.uk
 * @copyright De Montfort University
 *
 * @package simple_sums
 */

class DataView extends WebPageTemplateView
{
    private $page_content;
    private $calculation_result_message;

    public function __construct()
    {
        parent::__construct();
        $this->page_content = array();
        $this->calculation_result_message = '';
    }

    public function setOutputValues($page_content)
    {
//var_dump($page_content);
        $this->page_content = $page_content['stored-data'];
    }

    public function createOutputPage()
    {
        $this->createDataTable();
        $this->setPageTitle();
        $this->createPageBody();
        $this->createWebPage();
    }

    public function getHtmlOutput()
    {
        return $this->html_page_output;
    }

    private function setPageTitle()
    {
        $this->page_title = 'Stored Data...';
    }

    private function createDataTable()
    {
        $output = '';

        if ($this->page_content['data-retrieval-error'] === true)
        {
            $output = 'Oooops -  there was a problem retrieving the data from the database.  Please try again';
        }
        else
        {
            array_shift($this->page_content);
            $table_rows = '';
            foreach ($this->page_content as $values)
            {
                $table_rows .= '<tr>';
                $table_rows .= '<td>' . $values['client_port'] . '</td>';
                $table_rows .= '<td>' . $values['user_agent'] . '</td>';
                $table_rows .= '<td>' . $values['calculation_type'] . '</td>';
                $table_rows .= '<td>' . $values['value1'] . '</td>';
                $table_rows .= '<td>' . $values['value2'] . '</td>';
                $table_rows .= '<td>' . $values['timestamp'] . '</td>';
                $table_rows .= '</tr>';
            }

            $output = <<< HTMLTABLE
<table>
<tr>
<th class="head">Client Port</th><th>User Agent</th><th>Calculation Type</th><th>Value 1</th><th>Value 2</th><th>Timestamp</th>
</tr>
$table_rows
</table>
HTMLTABLE;

        }
        $this->calculation_result_message = $output;
    }

    private function createPageBody()
    {
        $address = APP_ROOT_PATH;
        $page_heading = 'Simple Arithmetic';
        $page_heading_2 = 'Stored Data';
        $stored_data_table = $this->calculation_result_message;
        $html_output = <<< HTML
<h2>$page_heading</h2>
<h3>$page_heading_2</h3>
<p>$stored_data_table</p>
<p><a href="$address">Try again</a></p>
HTML;
        $this->html_page_content = $html_output;
    }
}
