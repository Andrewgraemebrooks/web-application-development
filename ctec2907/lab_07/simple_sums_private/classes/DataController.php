<?php
/**
 * DataController.php
 *
 * PHP program to perform simple calculations
 *
 * class to retrieve data ....
 *
 * @author CF Ingrams - cfi@dmu.ac.uk
 * @copyright De Montfort University
 *
 * @package simple_sums
 */

class DataController
{
    private $html_output;
    private $sanitised_input;
    private $page_content;

    public function __construct()
    {
        $this->html_output = '';
        $this->sanitised_input = array();
        $this->page_content = array();
    }

    public function __destruct(){}

    public function getHtmlOutput()
    {
        return $this->html_output;
    }

    public function createOutput()
    {
        $this->retrieveStoredData();
        $this->createHtmlOutput();
    }

    public function retrieveStoredData()
    {
        $page_content = [];
        $model = Factory::buildObject('DataModel');
        $model->retrieveData();

        $stored_data = $model->getStoredData();

        $page_content['stored-data'] = $stored_data;
        $this->page_content = $page_content;
    }

    public function createHtmlOutput()
    {
        $view = Factory::buildObject('DataView');
        $view->setOutputValues($this->page_content);
        $view->createOutputPage();
        $this->html_output = $view->getHtmlOutput();
    }
}
