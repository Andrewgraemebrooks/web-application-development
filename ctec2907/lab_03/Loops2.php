<?php

// Set the panda counter to zero.
$pandas = 0;

// Iterate...
do {

    // Output the number of panda butlers.
    echo "We have {$pandas} panda butlers!<br>";

    // Increment the counter.
    $pandas++;

// ... while $pandas less than 50.
} while ($pandas < 50);