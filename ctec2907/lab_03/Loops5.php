<?php

// Create an array of pandas.
$pandas = [
    'first'     => 'Lushui',
    'second'    => 'Pali',
    'third'     => 'Jasmina'
];

// Iterate our panda array.
foreach ($pandas as $position => $panda) {

    // If the position variable equals 'second'.
    if ($position == 'second') {

        // Break the iteration.
        continue;
    }

    // Output.
    echo "You are the {$position} panda, {$panda}!\n";
}