<?php
/** index.php
	* PHP program to demonstrate the usage of the bcrypt and libsodium libraries
	*/

$tracing = true;

if (function_exists('xdebug_start_trace') && $tracing === true)
{
    $output_trace_dir = ini_get('xdebug.trace_output_dir');
    ini_set('display_errors', 'on');
    ini_set('html_errors', 'on');
    ini_set('xdebug.cli_color', 2);

    $output_trace_file_name = $output_trace_dir . '/libsodium.%t';

    xdebug_start_trace($output_trace_file_name);
}
	include 'simple_encryption_and_hashing/bootstrap.php';

if (function_exists('xdebug_stop_trace') && $tracing === true)
{
    xdebug_stop_trace();
}
