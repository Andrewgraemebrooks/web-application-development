<?php
 /**
  * @author CF Ingrams <cfi@dmu.ac.uk>
  * @copyright De Montfort University
  *
  * @package simple_encryption_and_hashing

  */

 class FormView extends WebPageTemplateView
 {

  public function __construct()
  {
   parent::__construct();
  }

  public function __destruct(){}

  public function createForm()
  {
   $this->setPageTitle();
   $this->createPageBody();
   $this->createWebPage();
  }

  public function getHtmlOutput()
  {
   return $this->html_page_output;
  }

  private function setPageTitle()
  {
   $this->page_title = 'Using the PHP LibSodium and Bcrypt Libraries';
  }

  private function createPageBody()
  {
   $address = APP_ROOT_PATH;
   $input_box_value = null;
   $max_input_string_length = MAX_INPUT_STRING_LENGTH;

   $page_heading = 'Encryption/Decryption';
   $page_heading_2 = 'Enter text to be encrypted and hashed (maximum length = ' . $max_input_string_length . ' characters)';
   $html_output = <<< HTMLFORM
<h2>$page_heading</h2>
<h3>$page_heading_2</h3>
<form id="form" action="$address" method="post">
<p class="curr_page"></p>
<input type="hidden" name="feature" value="display-result" />
<fieldset>
<legend>Text for encryption</legend>
<label for="string-to-encrypt">Enter your text:</label>
<input id="string-to-encrypt" name="string-to-encrypt" type="text"
value="$input_box_value" size="$max_input_string_length" maxlength="$max_input_string_length" />
<input type="submit" name="submit" value="Encrypt the text string >>>" />
</fieldset>
</form>
HTMLFORM;
   $this->html_page_content = $html_output;
  }
 }
