<?php
/**
 * @author CF Ingrams <cfi@dmu.ac.uk>
 * @copyright De Montfort University
 *
 * @package cryptodemo
 */

class ResultView extends WebPageTemplateView
{
    private $page_content;
    private $calculation_result_message;

    public function __construct()
    {
        parent::__construct();
        $this->page_content = [];
        $this->calculation_result_message = '';
    }

    public function __destruct() {}

    public function setOutputValues($page_content)
    {
        $this->page_content = $page_content;
    }

    public function createOutputPage()
    {
        $this->setPageTitle();
        $this->getResultMessage();
        $this->createWebPage();
    }

    public function getHtmlOutput()
    {
        return $this->html_page_output;
    }

    private function setPageTitle()
    {
        $this->page_title = APP_NAME . ' Processing result...';
    }

    private function getResultMessage()
    {
        if ($this->page_content['validation-result'] !== false)
        {
            $this->createSuccessMessage();
        }
        else
        {
            $this->createErrorMessage();
        }
        $this->html_page_content = $this->page_content['html'];
    }

    private function createErrorMessage()
    {
        $this->page_content['html'] = <<< HTMLBODY
<p class="curr_page">The text string you entered was not valid</p>
HTMLBODY;
    }

    private function createSuccessMessage()
    {
        $process_result = $this->page_content;
        $entered_string = $_POST['string-to-encrypt'];
        $sanitised_string = $process_result['validation-result'];
        $base64_encrypted_string = $process_result['base64-encoded-string'];
        $base64_decrypted_string = $process_result['base64-decoded-string'];
        $encrypted_string = $process_result['encrypted-text']['encrypted-string'];
        $encrypted_string = $process_result['encrypted-text']['nonce'];
        $decrypted_string = $process_result['decrypted-text'];

        $bcrypt_hashed_password = $process_result['hashed-password'];
        $authenticated_password = $process_result['authenticated-password'];
        if ($authenticated_password)
        {
            $password_authenticated = 'password ok';
        }
        else
        {
            $password_authenticated = 'password NOT ok';
        }

        $this->page_content['html'] = <<< HTMLBODY
<div id="page-content-div">
<p>Entered string: $entered_string</p>
<p>Sanitised string is: $sanitised_string</p>
<h2>Bcrypt</h2>
<p>bcrypt-hashed password: $bcrypt_hashed_password</p>
<p>Authenticated password: $password_authenticated</p>
<h2>libSodium </h2>
<p>Base 64 encoding of the LibSodium encrypted string: $base64_encrypted_string</p>
<p>Base 64 decoding of the encrypted string: $base64_decrypted_string</p>
<p>Encrypted string is: $encrypted_string</p>
<p>Decrypted string is: $decrypted_string</p>
</div>
HTMLBODY;
    }
}
