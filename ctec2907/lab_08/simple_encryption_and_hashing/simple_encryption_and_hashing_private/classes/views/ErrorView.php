<?php
/**
 * @author CF Ingrams <cfi@dmu.ac.uk>
 * @copyright De Montfort University
 *
 * @package cryptodemo
 */

class ErrorView extends WebPageTemplateView
{
 private $error_type;
 private $error_message;

 public function __construct()
 {
  parent::__construct();
  $this->error_type = '';
  $this->error_message = '';
 }

 public function __destruct(){}

 public function set_error_type($error_type)
 {
  $this->error_type = $error_type;
 }

 public function create_error_message()
 {
  $this->set_page_title();
  $this->select_error_message();
  $this->create_page_body();
  $this->createWebPage();
  $this->log_error_message();
 }

 public function get_html_output()
 {
  return $this->html_page_output;
 }

 private function set_page_title()
 {
  $this->page_title = APP_NAME . ' processing error...';
 }

 private function select_error_message()
 {
  switch ($this->error_type)
  {
   case 'file-not-found-error':
   case 'class-not-found-error':
   default:
    $error_message = 'Ooops - there was an internal error - please try again later';
    break;
  }
  $this->error_message = $error_message;
 }

 private function log_error_message()
 {
  trigger_error('Error type is: ' . $this->error_type);
 }

 private function create_page_body()
 {
  $address = APP_ROOT_PATH;

  $page_heading = 'Encryption/Decryption';
  $page_heading_2 = 'Error in processing';

  $error_message = $this->error_message;
  $html_output = <<< HTML
<h2>$page_heading</h2>
<h3>$page_heading_2</h3>
<p>$error_message</p>
<p><a href="$address">Try again</a></p>
HTML;
  $this->html_page_content = $html_output;
 }
}
