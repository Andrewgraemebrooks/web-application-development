<?php
/**
 * @author CF Ingrams <cfi@dmu.ac.uk>
 * @copyright De Montfort University
 *
 * @package cryptodemo
 */

class LibSodiumResultModel
{
  private $validate_error_flag;
  private $string_to_encrypt;
  private $result;
  private $encrypt_wrapper_handle;

  public function __construct()
  {
    $this->validate_error_flag = false;
    $this->string_to_encrypt = null;
    $this->result = [];
    $this->encrypt_wrapper_handle = null;
  }

  public function __destruct(){}

  public function getEncryptedResult()
  {
    return $this->result;
  }

  public function setEncryptValues($validate_result)
  {
    $this->string_to_encrypt = $validate_result['sanitised_text_string'];
  }

  public function setCryptWrapperHandle($encrypt_wrapper_handle)
  {
    $this->encrypt_wrapper_handle = $encrypt_wrapper_handle;
  }

  public function encryptionProcessing()
  {
    $process_result = [];
    $process_result['sanitised_string'] = null;
    $process_result['encrypted_string'] = null;
    $process_result['decrypted_string'] = null;

    if ($this->validate_error_flag)
    {
      $process_result['process_error'] = true;
    }
    else
    {
      $string_to_encrypt = $this->string_to_encrypt;

      $encrypted_string = $this->encrypt_wrapper_handle->encrypt($string_to_encrypt);
      $decrypted_string = $this->encrypt_wrapper_handle->decrypt($encrypted_string);

      $process_result['sanitised_string'] = $string_to_encrypt;
      $process_result['encrypted_string'] = $encrypted_string;
      $process_result['decrypted_string'] = $decrypted_string;
    }

    $this->result = $process_result;
  }
}
