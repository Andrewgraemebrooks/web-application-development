<?php
/**
 * @author CF Ingrams <cfi@dmu.ac.uk>
 * @copyright De Montfort University
 *
 * @package cryptodemo
 */

class BcryptResultModel
{
    private $validate_error_flag;
    private $bcrypt_string;
    private $result;

    public function __construct()
    {
        $this->validate_error_flag = false;
        $this->bcrypt_string = null;
        $this->result = [];
    }

    public function __destruct(){}

    public function get_encrypted_result()
    {
        return $this->result;
    }

    public function set_encrypt_values($bcrypt_validate_result)
    {
        $this->bcrypt_string = $bcrypt_validate_result['sanitised_text_string'];
    }

    public function do_create_hashed_password_bcrypt()
    {
        $password_to_hash = $this->bcrypt_string;
        $bcrypt_hashed_password = '';
        if ($password_to_hash != '')
        {
            $options = array('cost' => BCRYPT_COST);
            $bcrypt_hashed_password = password_hash($password_to_hash, BCRYPT_ALGO, $options);
        }
        $this->result['bcrypt-hashed-password'] = $bcrypt_hashed_password;
    }

    public function do_authenticate_password_bcrypt()
    {
        $user_authenticated = false;
        $current_user_password = $this->bcrypt_string;
        $stored_user_password_hash = $this->result['bcrypt-hashed-password'];
        if (password_verify($current_user_password, $stored_user_password_hash))
        {
            $user_authenticated = true;
        }
        $this->result['password-authenticated'] = $user_authenticated;
    }
}
