<?php
/**
 * Factory.php
 * PHP web application to demonstrate how databases are accessed securely
 *
 * @author CF Ingrams - cfi@dmu.ac.uk
 * @copyright De Montfort University
 *
 * @package petshow
 */

class Factory
{
    public function __construct(){}

    public function __destruct(){}

    /**
     * build any object and return a resource handle to the object
     */
    public static function buildObject($class)
    {
        $object = new $class();
        return $object;
    }
}
