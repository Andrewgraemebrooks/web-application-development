<?php
 /**
  * @author CF Ingrams <cfi@dmu.ac.uk>
  * @copyright De Montfort University
  *
  * @package cryptodemo
  */

 class WebPageTemplateView
 {
  protected $page_title;
  protected $html_page_content;
  protected $html_page_output;

  public function __construct()
  {
   $this->page_title = '';
   $this->html_page_content = '';
   $this->html_page_output = '';
  }

  public function __destruct(){}

  public function createWebPage()
  {
   $this->createWebPageMetaHeadings();
   $this->insertPageContent();
   $this->createWebPageFooter();
  }

  public function insertPageContent()
  {
   $landing_page = APP_ROOT_PATH;
   $app_name = APP_NAME;
   $html_output = <<< HTML
<div id="banner-div">
<h1>$app_name Processing</h1>
<p class="cent">
Page last updated on <script type="text/javascript">document.write(document.lastModified)</script>
<br />
Maintained by <a href="mailto:cfi@dmu.ac.uk">cfi@dmu.ac.uk</a>
</p>
<hr class="deepline"/>
</div>
<div id="clear-div"></div>
<div id="page-content-div">
$this->html_page_content
<p class="curr_page"><a href="$landing_page">Enter another text string</a></p>
</div>
HTML;
   $this->html_page_output .= $html_output;
  }

  public function createWebPageMetaHeadings()
  {
   $css_filename = CSS_PATH_AND_FILE_NAME;

   $html_output = <<< HTML
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta http-equiv="Content-Language" content="en-gb" />
<meta name="author" content="Clinton Ingrams" />
<link rel="stylesheet" href="$css_filename" type="text/css" />
<title>$this->page_title</title>
</head>
<body>
HTML;
   $this->html_page_output .= $html_output;
  }

  public function createWebPageFooter()
  {
   $html_output = <<< HTML
</body>
</html>
HTML;
   $this->html_page_output .= $html_output;
  }
 }
