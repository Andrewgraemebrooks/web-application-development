<?php
/**
 * Created by PhpStorm.
 * User: ctec2907
 * Date: 11/03/19
 * Time: 22:43
 */

class Base64Wrapper
{
    public function __construct(){}

    public function __destruct(){}

    public function encode_base64($string_to_encode)
    {
        $encoded_string = false;

        if (!empty($string_to_encode['encrypted-string']))
        {
            $nonce = $string_to_encode['nonce'];
            $encrypted_string = $string_to_encode['encrypted-string'];
            $encoded_string = base64_encode($nonce . $encrypted_string);
        }

        return $encoded_string;
    }

    public function decode_base64($string_to_decode)
    {
        $decoded_string = false;
        if (!empty($string_to_decode))
        {
            $decoded_string = base64_decode($string_to_decode);
        }
        return $decoded_string;
    }
}
