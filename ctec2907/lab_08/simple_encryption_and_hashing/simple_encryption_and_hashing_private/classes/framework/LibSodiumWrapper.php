<?php
/**
 * Wrapper class for the PHP libsodium library.  Takes the pain out of using the library.
 *
 * See https://paragonie.com/book/pecl-libsodium
 *
 * Methods available are:
 *
 * Encrypt/decrypt the given string
 * Encrypt/Decrypt the given string with base 64 encoding
 *
 * @author CF Ingrams <cfi@dmu.ac.uk>
 * @copyright De Montfort University
 *
 * @package cryptodemo
 */

/*
 * Th code depends on the mb_strr library.  You will need to install this in a terminal
 *
 * sudo apt-get install php7.2-mbstring
 *
 * sudo systemctl restart apache2
 */
/*class LibSodiumWrapper
{
  private $key;
  private $nonce;

  public function __construct()
  {
    // Generating an encryption key
    $this->key = \Sodium\randombytes_buf(\Sodium\CRYPTO_SECRETBOX_KEYBYTES);

    // Using a key to encrypt information
    $this->nonce = \Sodium\randombytes_buf(\Sodium\CRYPTO_SECRETBOX_NONCEBYTES);

  }

  public function __destruct(){}

  /**
   * https://paragonie.com/book/pecl-libsodium/read/04-secretkey-crypto.md#crypto-secretbox
   *
   * @param $string_to_encrypt
   * @return string
   */
/*  public function encrypt($string_to_encrypt)
  {
    $encrypted_string = \Sodium\crypto_secretbox($string_to_encrypt, $this->nonce, $this->key);

    return $encrypted_string;
  }

  public function decrypt($string_to_decrypt)
  {
    $decrypted_string = \Sodium\crypto_secretbox_open($string_to_decrypt, $this->nonce, $this->key);
    if ($decrypted_string === false)
    {
      throw new Exception("Bad ciphertext");
    }

    return $decrypted_string;
  }
}*/
class LibSodiumWrapper
{
    private $key;

    public function __construct()
    {
        $this->initialise_encryption();
    }

    public function __destruct()
    {
        sodium_memzero($this->key);
    }

    private function initialise_encryption()
    {
        $this->key = 'The boy stood on the burning dek';

        if (mb_strlen($this->key, '8bit') !== SODIUM_CRYPTO_SECRETBOX_KEYBYTES) {
            throw new RangeException('Key is not the correct size (must be 32 bytes).');
        }
    }

    public function encrypt($string_to_encrypt)
    {
        $nonce = random_bytes(SODIUM_CRYPTO_SECRETBOX_NONCEBYTES);

        $encryption_data = [];

        $encrypted_string = '';
        $encrypted_string = sodium_crypto_secretbox(
            $string_to_encrypt,
            $nonce,
            $this->key
        );

        $encryption_data['nonce'] = $nonce;
        $encryption_data['encrypted-string'] = $encrypted_string;

        sodium_memzero($string_to_encrypt);
        return $encryption_data;
    }

    public function decrypt($string_to_decrypt)
    {
        $decrypted_string = '';
//        $decoded = $base64_wrapper->decode_base64($string_to_decrypt);


        if (mb_strlen($string_to_decrypt, '8bit') < (SODIUM_CRYPTO_SECRETBOX_NONCEBYTES + SODIUM_CRYPTO_SECRETBOX_MACBYTES))
        {
            throw new Exception('Oops, the message was truncated');
        }

        $nonce = mb_substr($string_to_decrypt, 0, SODIUM_CRYPTO_SECRETBOX_NONCEBYTES, '8bit');

        $ciphertext = mb_substr($string_to_decrypt, SODIUM_CRYPTO_SECRETBOX_NONCEBYTES, null, '8bit');

        $decrypted_string = sodium_crypto_secretbox_open(
            $ciphertext,
            $nonce,
            $this->key
        );

        if ($decrypted_string === false)
        {
            throw new Exception('the message was tampered with in transit');
        }

        sodium_memzero($ciphertext);

        return $decrypted_string;
    }
}
