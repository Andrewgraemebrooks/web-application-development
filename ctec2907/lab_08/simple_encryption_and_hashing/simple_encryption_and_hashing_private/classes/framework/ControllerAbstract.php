<?php
 /**
  * @author CF Ingrams <cfi@dmu.ac.uk>
  * @copyright De Montfort University
  *
  * @package simple_encryption_and_hashing
  */

 abstract class ControllerAbstract
 {
  protected $html_output;
  protected $container;

  public final function __construct()
  {
   $this->html_output = '';
  }

  public final function __destruct(){}

  public function getHtmlOutput()
  {
   return $this->html_output;
  }

  abstract protected function createHtmlOutput();
 }
