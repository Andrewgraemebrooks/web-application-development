<?php
/**
 * Router class - sets the POSTed feature name.  If feature name exists, map it to a local name,
 * then call the correct Constructor class
 *
 * @author CF Ingrams <cfi@dmu.ac.uk>
 * @copyright De Montfort University
 *
 * @package simple_encryption_and_hashing
 */

class Router
{
    private $feature_in;
    private $feature;
    private $html_output;

    public function __construct()
    {
        $this->feature_in = '';
        $this->feature = '';
        $this->html_output = '';
    }

    public function __destruct(){}

    public function routing()
    {
        $this->setFeatureName();
        $this->validateFeatureName();
        $this->selectController();
    }

    public function getHtmlOutput()
    {
        return $this->html_output;
    }

    private function setFeatureName()
    {
        if (isset($_POST['feature']))
        {
            $feature_in = $_POST['feature'];
        }
        else
        {
            if (isset($_GET['feature']))
            {
                $feature_in = $_GET['feature'];
            }
            else
            {
                $feature_in = 'index';
            }
        }
        $this->feature_in = $feature_in;
    }

    private function validateFeatureName()
    {
        $feature_exists = false;

        $features = array(
            'index' => 'form',
            'display-result' => 'result'
        );

        if (array_key_exists($this->feature_in, $features))
        {
            $this->feature = $features[$this->feature_in];
            $feature_exists =  true;
        }
        else
        {
            $this->feature = 'feature-error';
        }
        return $feature_exists;
    }

    public function selectController()
    {
        switch ($this->feature)
        {
            case 'form':
                $controller = Factory::buildObject('FormController');
                break;
            case 'result':
                $controller = Factory::buildObject('ResultController');
                break;
            case 'feature-error':
                $controller = Factory::buildObject('ErrorController');
                $controller->setErrorType('feature-not-found-error');
                break;
            default:
                $controller = Factory::buildObject('IndexController');
                break;
        }
        $controller->createHtmlOutput();
        $this->html_output = $controller->getHtmlOutput();
    }
}
