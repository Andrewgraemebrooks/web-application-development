<?php
/**
 * @author CF Ingrams <cfi@dmu.ac.uk>
 * @copyright De Montfort University
 *
 * @package cryptodemo
 */

class Validate
{
    private $tainted;
    private $cleaned;

    public function __construct()
    {
        $this->tainted = [];
        $this->cleaned = [];
    }

    public function __destruct(){}


    public function getSanitisedInput()
    {
        return $this->cleaned;
    }

    /*     public function do_sanitise_input()
      {
       $this->tainted = $_POST;
       $this->cleaned['validate_error'] = false;
       $error_count = 0;
       $max_input_string_length = MAX_INPUT_STRING_LENGTH;

       $tainted_text_string = $this->tainted['string-to-encrypt'];
       $decoded_text_string = html_entity_decode($tainted_text_string);

       $sanitised_text_string = filter_var($decoded_text_string, FILTER_SANITIZE_STRING, FILTER_FLAG_NO_ENCODE_QUOTES);

       if (empty($sanitised_text_string))
       {
        $error_count++;
       }

       if (strlen($sanitised_text_string) > $max_input_string_length)
       {
        $error_count++;
       }

       if ($error_count > 0)
       {
        $this->cleaned['validate_error'] = true;
       }
       else
       {
        $this->cleaned['sanitised_text_string'] = $sanitised_text_string;
       }
      }
    */
    public static function validateString(string $datum_name, array $tainted, int $min_length = null, int $max_length = null)
    {
        $validated_string = false;

        if (!empty($tainted[$datum_name]))
        {
            $string_to_check = $tainted[$datum_name];
            $sanitised_string = filter_var($string_to_check, FILTER_SANITIZE_STRING, FILTER_FLAG_NO_ENCODE_QUOTES);
            $sanitised_string_length = strlen($sanitised_string);

            if ($min_length <= $sanitised_string_length && $sanitised_string_length <= $max_length)
            {
                $validated_string = $sanitised_string;
            }
        }

        return $validated_string;
    }
}
