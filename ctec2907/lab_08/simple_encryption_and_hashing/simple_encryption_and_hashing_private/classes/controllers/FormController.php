<?php
/**
 * Form Controller class
 *
 * @author CF Ingrams <cfi@dmu.ac.uk>
 * @copyright De Montfort University
 *
 * @package simple_encryption_and_hashing

 */

class FormController extends ControllerAbstract
{
    public function createHtmlOutput()
    {
        $view = Factory::buildObject('FormView');
        $view->createForm();
        $this->html_output = $view->getHtmlOutput();
    }
}
