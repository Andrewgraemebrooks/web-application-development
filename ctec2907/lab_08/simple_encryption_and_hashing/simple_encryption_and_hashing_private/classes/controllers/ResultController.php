<?php
/**
 *
 * Don't instantiate the models class if input is invalid
 *
 * @author CF Ingrams <cfi@dmu.ac.uk>
 * @copyright De Montfort University
 *
 * @package simple_encryption_and_hashing
 */

class ResultController extends ControllerAbstract
{
    public function createHtmlOutput()
    {
        $page_content = [];

        $validated_encryption_text = $this->validate();

        if ($validated_encryption_text !== false)
        {
            $bcrypt_results = $this->bcryptResults($validated_encryption_text);
            $libsodium_results = $this->libSodiumResults($validated_encryption_text);
        }

        $page_content = array_merge($bcrypt_results, $libsodium_results);
        $page_content['validation-result'] = $validated_encryption_text;
        $this->html_output = $this->createView($page_content);
    }

    private function validate()
    {
        $validate = Factory::buildObject('Validate');
        $tainted = $_POST;
        $validated_encryption_text = false;
        $max_input_string_length = MAX_INPUT_STRING_LENGTH;
        $minx_input_string_length = 0;

        $validated_encryption_text = $validate->validateString('string-to-encrypt', $tainted, $minx_input_string_length, $max_input_string_length);

        return $validated_encryption_text;
    }

    private function bcryptResults($validated_encryption_text)
    {
        $output = [];
        $model = Factory::buildObject('BcryptWrapper');

        $hashed_password = $model->hashPassword($validated_encryption_text);
        $stored_hashed_password = $hashed_password;

        $authenticated_password = $model->validatePassword($validated_encryption_text, $stored_hashed_password);
        $output['hashed-password'] = $hashed_password;
        $output['authenticated-password'] = $authenticated_password;

        return $output;
    }

    private function libSodiumResults($validated_encryption_text)
    {
        $output = [];
        $model = Factory::buildObject('LibSodiumWrapper');

        $encrypted_text = $model->encrypt($validated_encryption_text);
        $encoded_text = $this->base64('encode', $encrypted_text);

        $decoded_text = $this->base64('decode', $encoded_text);
        $decrypted_text = $model->decrypt($decoded_text);

        $output['base64-encoded-string'] = $encoded_text;
        $output['base64-decoded-string'] = $decrypted_text;
        $output['encrypted-text'] = $encrypted_text;
        $output['decrypted-text'] = $decrypted_text;

        return $output;
    }

    private function base64($type, $text)
    {
        $output = false;
        $wrapper = Factory::buildObject('Base64Wrapper');
        if ($type == 'encode')
        {
            $output = $wrapper->encode_base64($text);
            $error_message = 'Ooops, the encoding failed';
        }
        else
        {
            $output = $wrapper->decode_base64($text);
            $error_message = 'Ooops, the decoding failed';
        }

        if ($output === false)
        {
            throw new Exception($error_message);
        }
        return $output;
    }

    private function createView($validated_encryption_text)
    {
        $view = Factory::buildObject('ResultView');
        $view->setOutputValues($validated_encryption_text);
        $view->createOutputPage();
        $html_output = $view->getHtmlOutput();

        return $html_output;
    }
}
