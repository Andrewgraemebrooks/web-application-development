<?php
/**
 * Created by PhpStorm.
 * User: cfi
 * Date: 13/03/16
 * Time: 14:58
 */

define ('DIRSEP', DIRECTORY_SEPARATOR);
define ('URLSEP', '/');

$app_file_path = realpath(dirname(__FILE__));
$class_file_path = $app_file_path . DIRSEP . 'classes' . DIRSEP;

$app_root_path = $_SERVER['PHP_SELF'];
$document_root = $_SERVER['HTTP_HOST'];
$app_root_path = explode(URLSEP, $app_root_path, -1);
$app_root_path = implode(URLSEP, $app_root_path) . URLSEP;
$app_root_path = 'http://' . $document_root . $app_root_path;

$max_input_string_length = 100;
$css_file_name = 'styles.css';
$css_file_path = $app_root_path . URLSEP . 'css';
$css_path_and_file_name = $css_file_path . URLSEP . $css_file_name;

$bcrypt_algorythm = PASSWORD_DEFAULT;
$bcrypt_cost = 12;

define ('CLASS_PATH', $class_file_path);
define ('APP_ROOT_PATH', $app_root_path);
define ('APP_NAME', 'Encryption &amp; Hashing');
define ('MAX_INPUT_STRING_LENGTH', $max_input_string_length);
define ('CSS_PATH_AND_FILE_NAME', $css_path_and_file_name);

define ('BCRYPT_ALGO', $bcrypt_algorythm);
define ('BCRYPT_COST', $bcrypt_cost);
