<?php
/**
 * index.php
 *
 * Petshow: PHP web application to demonstrate how databases are accessed securely
 *
 * NB the include_path setting in php.ini must be correctly set to access the sensitive application files
 *
 * @author CF Ingrams - cfi@dmu.ac.uk
 * @copyright De Montfort University
 *
 * @package petshow_2
 */

if (function_exists('xdebug_start_trace'))
{
    $output_trace_dir = ini_get('xdebug.trace_output_dir');
    ini_set('display_errors', 'on');
    ini_set('html_errors', 'on');
//    ini_set('xdebug.trace_output_name', 'petshow_2.%t');
    ini_set('xdebug.cli_color', 2);

    $output_trace_file_name = $output_trace_dir . '/petshow_2.%t';

    xdebug_start_trace($output_trace_file_name);
}

include 'petshow/bootstrap.php';

if (function_exists('xdebug_stop_trace'))
{
    xdebug_stop_trace();
}
