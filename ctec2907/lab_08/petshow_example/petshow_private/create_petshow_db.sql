/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50516
Source Host           : localhost:3306
Source Database       : db

Target Server Type    : MYSQL
Target Server Version : 50516
File Encoding         : 65001

Date: 2012-10-27 21:29:13
*/

SET FOREIGN_KEY_CHECKS=0;

DROP DATABASE IF EXISTS petshow_db;
CREATE DATABASE petshow_db;
USE petshow_db;

GRANT SELECT, INSERT, UPDATE, DELETE on petshow_db.* TO petshowuser@localhost IDENTIFIED BY 'petshowpass';

-- ----------------------------
-- Table structure for `pet`
-- ----------------------------
DROP TABLE IF EXISTS `pet`;
CREATE TABLE `pet` (
  `pet_index` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `pet_name` varchar(20) NOT NULL,
  `owner_initials` varchar(10) NOT NULL,
  `pet_type` varchar(20) NOT NULL,
  `pet_sex` varchar(1) NOT NULL,
  `pet_dob` date NOT NULL,
  `pet_is_alive` enum('N','Y') NOT NULL DEFAULT 'Y',
  `pet_do_death` date DEFAULT NULL,
  PRIMARY KEY (`pet_index`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of pet
-- ----------------------------
INSERT INTO `pet` VALUES ('1', 'Tiddles', 'rbc', 'cat', 'M', '2006-02-04', 'Y', null);
INSERT INTO `pet` VALUES ('2', 'Bonzo', 'pdn', 'dog', 'F', '2004-03-17', 'Y', null);
INSERT INTO `pet` VALUES ('3', 'Woof', 'rbc', 'dog', 'M', '1999-05-13', 'N', '2006-02-13');
INSERT INTO `pet` VALUES ('4', 'Suzy', 'rbc', 'cat', 'F', '2000-08-27', 'Y', null);
INSERT INTO `pet` VALUES ('5', 'Bowser', 'pdn', 'dog', 'M', '1998-08-31', 'N', '1995-07-29');
INSERT INTO `pet` VALUES ('6', 'Chirpy', 'ims', 'bird', 'F', '2002-09-11', 'Y', null);
INSERT INTO `pet` VALUES ('7', 'Whiskey', 'rbc', 'bird', 'M', '2003-12-09', 'Y', null);
INSERT INTO `pet` VALUES ('8', 'Slick', 'pm', 'snake', 'M', '2001-04-29', 'Y', null);
INSERT INTO `pet` VALUES ('9', 'Hammy', 'ims', 'hamster', 'F', '2006-03-30', 'Y', null);

-- ----------------------------
-- Table structure for `petpics`
-- ----------------------------
DROP TABLE IF EXISTS `petpics`;
CREATE TABLE `petpics` (
  `pet_index` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `pet_pic_source` varchar(255) DEFAULT NULL,
  `pet_description` text,
  PRIMARY KEY (`pet_index`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of petpics
-- ----------------------------
INSERT INTO `petpics` VALUES ('1', 'cat.gif', 'what a pretty pussy cat');
INSERT INTO `petpics` VALUES ('2', 'bulldog.gif', 'I\'m rock');
INSERT INTO `petpics` VALUES ('3', 'dog4.gif', 'I am sooo cute');
INSERT INTO `petpics` VALUES ('4', 'alleycat.gif', 'bite me');
INSERT INTO `petpics` VALUES ('5', 'bloodhound.gif', 'my cousin is a Baskerville');
INSERT INTO `petpics` VALUES ('6', 'ostrich.gif', 'sand - what sand?');
INSERT INTO `petpics` VALUES ('7', 'vulture.gif', 'I have my eye on you');
INSERT INTO `petpics` VALUES ('8', 'snake.gif', 'here be sid');
INSERT INTO `petpics` VALUES ('9', 'mouse.gif', 'first cousin to a hamster');

-- ----------------------------
-- Table structure for `error_log`
-- ----------------------------
DROP TABLE IF EXISTS `error_log`;
CREATE TABLE `error_log` (
  `log_index` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `log_message` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `log_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`log_index`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of error_log
-- ----------------------------
