<?php
/**
 * Validate.php  * Sessions: PHP web application to demonstrate how databases
 * are accessed securely
 *
 *
 * @author CF Ingrams - cfi@dmu.ac.uk
 * @copyright De Montfort University
 *
 * @package petshow
 */

class Validate
{
    private $tainted;
    private $cleaned;

    public function __construct()
    {
        $this->tainted = array();
        $this->cleaned = array();
    }

    public function __destruct(){}

    public function getSanitisedInput()
    {
        return $this->cleaned;
    }

    public static function validateString(string $datum_name, array $tainted, int $min_length = null, int $max_length = null)
    {
        $validated_string = false;
        if (!empty($tainted[$datum_name]))
        {
            $string_to_check = $tainted[$datum_name];
            $sanitised_string = filter_var($string_to_check, FILTER_SANITIZE_STRING, FILTER_FLAG_NO_ENCODE_QUOTES);
            $sanitised_string_length = strlen($sanitised_string);
            if ($min_length <= $sanitised_string_length && $sanitised_string_length <= $max_length)
            {
                $validated_string = $sanitised_string;
            }
        }
        return $validated_string;
    }

/*    public function sanitiseInput()
    {
        $no_petname_selected_error = true;
        $validated_pet_name = false;
        $this->tainted = $_POST;
        $valid_details = false;
        $error_count = 0;

        if (isset($this->tainted['pet-name']))
        {
            $pet_name_to_validate = $this->tainted['pet-name'];
            if ($pet_name_to_validate == '0')
            {
                $error_count += 1;
            }

            if (is_string($pet_name_to_validate) === false)
            {
                $error_count += 1;
            }
        }
        else
        {
            $error_count += 1;
        }

        if ($error_count == 0)
        {
            $no_petname_selected_error = false;
            $validated_pet_name = filter_var($pet_name_to_validate, FILTER_SANITIZE_STRING);
            $this->cleaned['sanitised-pet-name'] = $validated_pet_name;
        }

        $this->cleaned['no-petname-selected-error'] = $no_petname_selected_error;
    }*/
}
