<?php
/**
 * Router.php  * Sessions: PHP web application to demonstrate how databases
 * are accessed securely
 *
 *
 * @author CF Ingrams - cfi@dmu.ac.uk
 * @copyright De Montfort University
 *
 * @package petshow
 */

class Router
{
    private $feature_in;
    private $feature;
    private $html_output;

    public function __construct()
    {
        $this->feature_in = '';
        $this->feature = '';
        $this->html_output = '';
    }

    public function __destruct(){}

    public function routing()
    {
        $this->setFeatureName();
        $this->mapFeatureName();
        $this->selectController();
    }

    public function getHtmlOutput()
    {
        return $this->html_output;
    }

    private function setFeatureName()
    {
        if (isset($_POST['feature']))
        {
            $feature_in = $_POST['feature'];
        }
        else
        {
            $feature_in = 'index';
        }
        $this->feature_in = $feature_in;
    }

    private function mapFeatureName()
    {
        // map the passed module name to an internal application feature name
        $features = array(
            'index' => 'index',
            'show_pet_names' => 'show-pet-names',
            'display_pet_details' => 'display-pet-details'
        );

        if (array_key_exists($this->feature_in, $features))
        {
            $this->feature = $features[$this->feature_in];
        }
        else
        {
            $this->feature = 'feature-error';
        }
    }

    public function selectController()
    {
        switch ($this->feature)
        {
            case 'show-pet-names':
                $controller = Factory::buildObject('ListPetNamesController');
                break;
            case 'display-pet-details':
                $controller = Factory::buildObject('DisplayPetDetailsController');
                break;
            case 'feature-error':
                $controller = Factory::buildObject('ErrorController');
                $controller->setErrorType('feature-not-found-error');
                break;
            default:
                $controller = Factory::buildObject('IndexController');
                break;
        }
        $controller->createHtmlOutput();
        $this->html_output = $controller->getHtmlOutput();
    }
}
