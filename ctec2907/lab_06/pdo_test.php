<?php
/**
 * Created by PhpStorm.
 * User: ctec2907
 * Date: 03/02/19
 * Time: 17:34
 */

$pdo_dsn = 'mysql:host=localhost;dbname=pdo_test';
$pdo_user_name = 'pdo_tester';
$pdo_user_password = 'password';

$db_link = new PDO($pdo_dsn, $pdo_user_name, $pdo_user_password);

$username = 'freddy';
$stmt = $db_link->prepare('select username, password from users where username = :keyusername');
//$stmt2 = $db_link->prepare("select * from users where 1=:key");
$stmt->bindParam(':keyusername', $username);

$pdo_result = $stmt->execute();

//$pdo_result = $stmt->execute(
//    array(
//        ':keyusername' => $username,
//        ':password' => $password
//    )
//);

$row_count = $stmt->rowCount();
$all_data = $stmt->fetchAll();
$username = $all_data[0][0];
$password = $all_data[0][1];

var_dump($pdo_result);
var_dump($row_count);
var_dump($all_data);
var_dump($username);
var_dump($password);
