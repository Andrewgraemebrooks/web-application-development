<?php
/**
 * View class definition
 *
 * setter methods are used to assign values for embedding within the output html string
 *
 * @author Clinton Ingrams
 * @date 12th March 2016
 * @package list_databases
 */

class View
{
    private $page_content;
    private $page_title;
    private $html_page;

    public function __construct( ) { }

    /**
     * assign the page title to a class attribute
     *
     * @param $page_title
     */
    public function setPageTitle($page_title)
    {
        $this->page_title = $page_title;
    }

    /**
     * assign the page content to a class attribute
     *
     * @param $page_content
     */
    public function setPageContent($page_content)
    {
        $this->page_content = $page_content;
    }

    /**
     * adds the head of the html page with the page title embedded
     *
     */
    public function createHtmlPage()
    {
        $this->html_page = <<< HTML
<!DOCTYPE html>
<html lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="Author" content="Clinton Ingrams" />
	<title>$this->page_title</title>
</head>
<body>
	$this->page_content
</body>
</html>
HTML;
    }

    /**
     * returns the completed html page
     * @return mixed
     */
    public function getHtmlPage()
    {
        return $this->html_page;
    }
}
