<?php
/**
 * Main code for the application.  The  process is structured in functions
 * Values are passed aS parameters into functions for processing
 *
 * The page heading is declared as a static string
 *
 * NB this example connects to the database as root user
 * Never do this again - it is intrinsically unsafe!
 *
 * @author Clinton Ingrams
 * @date 12th March 2016
 * @package list_databases
 */

define ('PAGE_HEADING', 'List of Databases & Tables');

require_once 'View.php';

$db_handle = connectToDatabase();
$table_output = createTableOutput($db_handle);
$html_output = createPageContent($table_output, $db_handle);
$final_html_output = createHtmlPage($html_output);

echo $final_html_output;

// close database connection
$db_handle = null;

// end of application code

/**
 * NB the value returned should be tested for validity before further use.
 *
 * @return null|PDO
 */
function connectToDatabase()
{
	$db_link = null;
	require_once 'mysql-root-connect.php';

	try
	{
		$db_link = new PDO($pdo_dsn, $pdo_user_name, $pdo_user_password, array(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION));
	}
	catch (PDOException $error)
	{
		print 'Error!: ' . $error->getMessage() . '<br>';
		exit();
	}
	return $db_link;
}

function createTableOutput($local_db_link)
{
	$table_output = '';

	$query1 = 'show databases;';
	$db_names = $local_db_link->query($query1);

	while ($db_name = $db_names->fetch(PDO::FETCH_NUM))
	{
		$table_output .= '<p>';
		$database_name_to_use = $db_name[0];
		$table_output .= $database_name_to_use;
		$table_output .= '<br>';

		$query2 = 'use ' . $database_name_to_use . ';';
		$select_database = $local_db_link->query($query2);
		$query3 = 'show tables;';
		$database_table_names = $local_db_link->query($query3);

		while ($database_table_name = $database_table_names->fetch(PDO::FETCH_NUM))
		{
			$table_output .= '--' . $database_table_name[0] . '<br>';
		}

		$row_count = $database_table_names->rowCount();
		$table_output .= '==> ' . $row_count . ' table(s) in ' . $database_name_to_use . '</p>';
	}
	return $table_output;
}

function createPageContent($table_output, $db_link)
{
	$html_output = '';
	$html_output = '<h2>' . PAGE_HEADING . '</h2>';
	$html_output .= '<p>Host information: ' . $db_link->getAttribute(constant("PDO::ATTR_SERVER_VERSION")) . '</p>';
	$html_output .= '<p>Server metrics: ' . $db_link->getAttribute(constant("PDO::ATTR_SERVER_INFO")) . '</p>';
	$html_output .= $table_output;
	return $html_output;
}

function createHtmlPage($html_output)
{
	$result_page = new View();

	$result_page->setPageTitle(PAGE_HEADING);
	$result_page->setPageContent($html_output);
	$result_page->createHtmlPage();
	$final_html_output = $result_page->getHtmlPage();
	return $final_html_output;
}
