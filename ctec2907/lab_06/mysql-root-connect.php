<?php
/**
 * Settings required for database connection
 *
 * NB this example connects to the database as root user
 * Never do this again - it is intrinsically unsafe!
 *
 * @author Clinton Ingrams
 * @date 12th March 2016
 * @package list_databases
 */

	$rdbms = 'mysql';
	$host = 'localhost';
	$charset = 'utf8mb4';
	$db_name = '';
	$pdo_dsn = $rdbms . ':host=' . $host . ';dbname=' . $db_name . ';charset=' . $charset;
	$pdo_user_name = 'root2';
	$pdo_user_password = 'password';
