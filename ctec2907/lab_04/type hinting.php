<?php
function sayHello(array $names) {
    foreach ($names as $name) {
        echo "Hello, {$name}!\n";
    }
}

sayHello(['Andrew','Bill']);