<?php

function welcome() {

    // Get all function parameters.
    $names = func_get_args();

    // Iterate the names and welcome them.
    foreach ($names as $name) {
        echo "Welcome, {$name}!\n";
    }
}

welcome("andrew", "bill");